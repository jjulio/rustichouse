// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  serverUrl: 'http://localhost:49220',
  port: '49220',
  percentTips:'10',
  STATE_CONFIRMED: '2',
  STATE_CANCELED: '4',
  getAllDataPath: '/login/getAllData',
  createOrderPath: '/payOffice/createOrder',
  updateOrderPath: '/orders/updateOrder',
  authPath: '/login/authenticate',
  getAllTablePath: '/tables/getAllTables',
  getAllOrdersActives: '/payOffice/getAllOrdersActives',
  updateStateOrder: '/payOffice/updateStateOrder',
  printTicket: '/payOffice/printOrder',
  getOrderById: '/payOffice/getOrder'
};
