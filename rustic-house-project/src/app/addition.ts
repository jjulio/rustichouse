export class Addition {
    Id: number;
    Name: string;
    Price: number;
    ImagePath: string;
    Description: string;
    Enable: boolean;

    constructor() {
        this.Id = 0;
        this.Price = 0;
    }   
}
