import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DoOrdersComponent } from './do-orders.component';

describe('DoOrdersComponent', () => {
  let component: DoOrdersComponent;
  let fixture: ComponentFixture<DoOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DoOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
