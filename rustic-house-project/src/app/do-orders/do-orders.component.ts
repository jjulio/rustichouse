import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

import { DoOrdersService } from '../do-orders.service';
import { Category } from '../category';
import { Product } from '../product';
import { Order } from '../order';
import { User } from '../user';
import { OrderDetail } from '../order-detail';
import { Addition } from '../addition';
import { SummaryItem } from '../summary-item';
import { Utility } from '../utility';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { TableSelectedService } from '../table-selected/table-selected.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Table } from '../models/table';
import { State } from '../state';

@Component({
  selector: 'app-do-orders',
  templateUrl: './do-orders.component.html',
  styleUrls: ['./do-orders.component.css'],
  providers: [DoOrdersService, AuthenticationService, TableSelectedService]
})

export class DoOrdersComponent implements OnInit {
  private categories: Category[];
  private categorySelected: Category;
  private showProducts: boolean;
  private clientOrder: Order;
  private orderWithAddition: Array<SummaryItem>;
  private orderWithOutAddition: Array<SummaryItem>;
  private categoryAdditions: Array<Addition>;
  private currentProduct: Product;
  private changeProduct: boolean;
  private userLogged: User;
  private updateOrder: boolean;
  private showAdditions: boolean;

  constructor(public orderService: DoOrdersService,
    private confirmationDialogService: ConfirmationDialogService,
    private authService: AuthenticationService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private toastr: ToastrService,
    private selectedTableService: TableSelectedService
  ) { 
    if (!this.validateUser()) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
    let orderId: number = 0;    

    if (this.activeRoute.snapshot.params.id != undefined) {
      orderId = this.activeRoute.snapshot.params.id;
      this.updateOrder = true;
    }

    this.categorySelected = new Category();
    this.clientOrder = new Order();
    this.getdata(orderId);
    this.orderWithOutAddition = new Array<SummaryItem>();
    this.orderWithAddition = new Array<SummaryItem>();
  }

  private validateUser(): boolean {
    this.userLogged = this.authService.getUserLoggedIn();
    return !this.userLogged || this.userLogged === null ? false : true;
  }

  private getProductsByCategory(currentCategory: Category) {
    this.categorySelected = currentCategory;
    this.showProducts = this.categorySelected.Products.length > 0 ? true : false;
    this.categoryAdditions = currentCategory.Additions;
    this.showAdditions = currentCategory.Additions.length > 0 ? true : false;
  }

  private getdata(id?: number) {
    this.getCategories();
    this.getOrderById(this.userLogged.Token, id);
  }

  private getCategories() {
    this.orderService.getAllData().subscribe(
      result => {
        this.categories = result;
        console.log(this.categories);
      },
      error => {
        console.log('Error:' + <any>error);
      }
    );
  }

  private getOrderById(token: string, id?: number) {
    if (id === 0) {
      return;
    }

    this.orderService.getOrder(id, token).subscribe(
      result => {
        if (result != null) {
          this.clientOrder = result;
          this.setSummaryItems();
          //console.log(this.clientOrder);
        }
      },
      (e: HttpErrorResponse) => {
        if (e.error instanceof Error) {
          console.log('Error:' + e.message);
        } else {
          this.toastr.error('Error obteniendo información del pedido', 'Orden de compra', {
            positionClass: 'toast-bottom-right'
          });
          console.log(e.message);
        }
      });
  }

  private changePropertyContainer(showProducts) {
    if (showProducts) {
      this.showProducts = false;
    }
  }

  private addToOrder(selectedProduct: Product) {
    // alert(`nombre: ${selectedProduct.Name} categoria: ${selectedProduct.Category.Name} precio: ${selectedProduct.Price}`);

    this.changeProduct = true;
    const summaryItem: SummaryItem[] = this.orderWithOutAddition.filter(x => x.ProductId === selectedProduct.Id);

    if (summaryItem.length > 0) {
      summaryItem[0].ProductAmount += 1;
      summaryItem[0].SummaryProductPurchase = summaryItem[0].SummaryProductPurchase + summaryItem[0].UnitPriceProduct;
    } else {
      const summary: SummaryItem = new SummaryItem();
      summary.ProductId = selectedProduct.Id;
      summary.ProductName = selectedProduct.Name;
      summary.ProductDescription = selectedProduct.Description;
      summary.ProductAmount = 1;
      summary.UnitPriceProduct = selectedProduct.Price;
      summary.SummaryProductPurchase = summary.UnitPriceProduct;
      this.orderWithOutAddition.push(summary);
    }

    const orderDetail: OrderDetail = new OrderDetail();
    this.currentProduct = Object.assign({}, selectedProduct);
    orderDetail.Product = this.currentProduct;
    this.clientOrder.OrderDetails.push(orderDetail);
    this.clientOrder.OrderTotal = SummaryItem.getSummaryOrderTotal(this.orderWithAddition, this.orderWithOutAddition);
    console.log('selectedProduct');
    console.log(selectedProduct);
    console.log(this.clientOrder);
  }

  private addAdditionToOrder(selectedAddition: Addition) {
    if (!this.orderWithAddition) {
      this.orderWithAddition = new Array<SummaryItem>();
    }

    this.currentProduct.Additions = this.currentProduct.Additions == null ? new Array<Addition>() : this.currentProduct.Additions;
    this.currentProduct.Additions.push(Object.assign({}, selectedAddition));

    if (this.changeProduct) {
      let summary = new SummaryItem();
      summary = this.fillSummaryItem(this.currentProduct, selectedAddition);
      this.deleteSummaryItem(this.currentProduct, this.orderWithOutAddition);
      this.orderWithAddition.push(summary);
    } else {
      const summaryItem: SummaryItem[] = this.orderWithAddition.filter(x => x.ProductId === this.currentProduct.Id);

      if (summaryItem.length > 0) {
        summaryItem[summaryItem.length - 1].AdditionsDisplay = summaryItem[summaryItem.length - 1].AdditionsDisplay.length === 0 ?
          selectedAddition.Name : summaryItem[summaryItem.length - 1].AdditionsDisplay + ', ' + selectedAddition.Name;
        summaryItem[summaryItem.length - 1].ProductAdditions.push(selectedAddition);
        summaryItem[summaryItem.length - 1].SummaryProductPurchase = SummaryItem.getSummaryPurchase(
          summaryItem[summaryItem.length - 1].ProductAdditions, summaryItem[summaryItem.length - 1].ProductAmount,
          summaryItem[summaryItem.length - 1].UnitPriceProduct);
      }
    }

    const groupBy = this.groupByOrderWithAddition();
    this.clientOrder.OrderTotal = SummaryItem.getSummaryOrderTotal(this.orderWithAddition, this.orderWithOutAddition);
    this.changeProduct = false;
  }

  private groupByOrderWithAddition(): Array<SummaryItem> {
    const summary: Array<SummaryItem> = new Array<SummaryItem>();

    if (this.orderWithAddition.length === 1) {
      return this.orderWithAddition;
    }

    for (let index1 = 0; index1 < this.orderWithAddition.length; index1++) {
      let sumOrder = new SummaryItem();
      let sumAdditions = 0;
      sumOrder = Object.assign({}, this.orderWithAddition[index1]);

      for (let index2 = index1 + 1; index2 < this.orderWithAddition.length; index2++) {
        if (this.orderWithAddition[index1].ProductId === this.orderWithAddition[index2].ProductId &&
          Utility.collectionCompare(sumOrder.ProductAdditions, this.orderWithAddition[index2].ProductAdditions)) {
          sumOrder.ProductAmount += 1;
          sumAdditions += SummaryItem.getSumAdditions(sumOrder.ProductAdditions) * 2;
          sumOrder.SummaryProductPurchase = (sumOrder.ProductAmount * sumOrder.UnitPriceProduct) + sumAdditions;
        }
      }

      const values = summary.filter(x => x.ProductId === this.orderWithAddition[index1].ProductId &&
        Utility.collectionCompare(x.ProductAdditions, sumOrder.ProductAdditions));
      if (values.length === 0) {
        summary.push(sumOrder);
      }
    }

    return summary;
  }

  private fillSummaryItem(currentProduct: Product, addition: Addition): SummaryItem {
    const summary: SummaryItem = new SummaryItem();
    summary.ProductId = currentProduct.Id;
    summary.ProductName = currentProduct.Name;
    summary.ProductDescription = currentProduct.Description;
    summary.ProductAmount = 1;
    summary.UnitPriceProduct = currentProduct.Price;
    summary.ProductAdditions.push(addition);
    summary.AdditionsDisplay = summary.AdditionsDisplay.length === 0 ? addition.Name : summary.AdditionsDisplay + ', ' + addition.Name;
    summary.SummaryProductPurchase = SummaryItem.getSummaryPurchase(
      summary.ProductAdditions, summary.ProductAmount, summary.UnitPriceProduct);
    return summary;
  }

  private deleteSummaryItem(product: Product, items: Array<SummaryItem>) {
    const products = items.filter(x => x.ProductId === product.Id);

    if (products.length > 0) {
      if (products[0].ProductAmount > 1) {
        products[0].ProductAmount = products[0].ProductAmount - 1;
        products[0].SummaryProductPurchase = products[0].ProductAmount * products[0].UnitPriceProduct;
      } else {
        const index = items.findIndex(x => x.ProductId === product.Id);
        if (index !== -1) {
          items.splice(index, 1);
        }
      }
    }
  }

  private createOrder() {
    if (this.clientOrder.OrderTotal === 0) {
      this.toastr.warning('No hay productos seleccionados', 'Orden de compra', {
        positionClass: 'toast-bottom-right'
      });

      return;
    }

    this.showSelectedTableComponent();
  }

  private orderCancel() {
    this.openConfirmationDialog();
  }

  private deleteProduct(item: SummaryItem) {
    let id: number = 0;
    // console.log('item');
    // console.log(item);
    // console.log('this.clientOrder');
    // console.log(this.clientOrder);

    if (item.ProductAdditions.length === 0) {
      id = this.clientOrder.OrderDetails.findIndex(x => x.Product.Id === item.ProductId);
      this.deleteProductWithOutAddition(item);
    } else {
      id = this.clientOrder.OrderDetails.reduce((x: any, od: OrderDetail) => {
        if (od.Product.Additions.length > 0 && od.Product.Id === item.ProductId) {
          return Utility.collectionCompare(od.Product.Additions, item.ProductAdditions) ? od.Id : -1;
        }
      }, 0);
      this.deleteProductWithAddition(item);
    }

    if (id !== -1) {
      // eliminar detalle en clientOrder
      this.clientOrder.OrderDetails.splice(id, 1);
    }

    this.clientOrder.OrderTotal = SummaryItem.getSummaryOrderTotal(this.orderWithAddition, this.orderWithOutAddition);
    this.currentProduct = this.clientOrder.OrderTotal === 0 ? null : this.currentProduct;

    console.log('nuevo clientOrder');
    console.log(this.clientOrder);
  }

  private deleteProductWithOutAddition(item: SummaryItem) {
    if (item.ProductAmount > 1) {
      item.ProductAmount--;
      item.SummaryProductPurchase = item.ProductAmount * item.UnitPriceProduct;
    } else {
      const index = this.orderWithOutAddition.findIndex(x => x.ProductId === item.ProductId);
      if (index !== -1) {
        this.orderWithOutAddition.splice(index, 1);
      }
    }
  }

  private deleteProductWithAddition(item: SummaryItem) {
    if (item.ProductAmount > 1) {
      item.ProductAmount--;
      item.SummaryProductPurchase = SummaryItem.getSummaryPurchase(item.ProductAdditions, item.ProductAmount, item.UnitPriceProduct);
    } else {
      const index = this.orderWithAddition.findIndex(x => x.ProductId === item.ProductId);
      if (index !== -1) {
        this.orderWithAddition.splice(index, 1);
      }
    }
  }

  private addProduct(item: SummaryItem) {
    if (item.ProductAdditions.length === 0) {
      this.addProductWithOutAddition(item);
    } else {
      this.addProductWithAddition(item);
    }

    let od: OrderDetail = new OrderDetail();
    od.Product.Id = item.ProductId;
    od.Product.Name = item.ProductName;
    od.Product.Price = item.UnitPriceProduct;
    od.Product.Description = item.ProductDescription;
    od.Product.Additions = item.ProductAdditions;
    this.clientOrder.OrderDetails.push(od);
    this.clientOrder.OrderTotal = SummaryItem.getSummaryOrderTotal(this.orderWithAddition, this.orderWithOutAddition);

    console.log(this.clientOrder);
  }

  private addProductWithOutAddition(item: SummaryItem) {
    item.ProductAmount += 1;
    item.SummaryProductPurchase = item.SummaryProductPurchase + item.UnitPriceProduct;
  }

  private addProductWithAddition(item: SummaryItem) {
    item.ProductAmount += 1;
    item.SummaryProductPurchase = SummaryItem.getSummaryPurchase(item.ProductAdditions, item.ProductAmount, item.UnitPriceProduct);
  }

  private cleanObjects() {
    this.orderWithAddition = [];
    this.orderWithOutAddition = [];
    this.clientOrder.OrderDetails = [];
    this.clientOrder.OrderTotal = 0;
    this.currentProduct = null;
  }

  private setSummaryItems() {
    var g = this.clientOrder.OrderDetails.reduce((x: any, od: OrderDetail) => {
      const item = new SummaryItem();
      item.ProductName = od.Product.Name;
      item.ProductId = od.Product.Id;
      item.UnitPriceProduct = od.Product.Price;

      if (od.Product.Additions.length > 0) {
        this.setSummaryItemWithAddtions(item, od);
      } else {
        this.setSummaryItemWithOutAddtions(item, od);
      }

      return x;
    }, {});

    this.getSummaryOrderTotal();
  }

  private setSummaryItemWithAddtions(item: SummaryItem, od: OrderDetail) {
    let sumAdditions = 0;
    const oa = this.orderWithAddition.filter(x => x.ProductId == od.Product.Id);

    if (oa.length > 0) {
      if (Utility.collectionCompare(oa[0].ProductAdditions, od.Product.Additions)) {
        oa[0].ProductAmount++;
        sumAdditions = SummaryItem.getSumAdditions(oa[0].ProductAdditions) * 2;
        oa[0].SummaryProductPurchase = (oa[0].ProductAmount * oa[0].UnitPriceProduct) + sumAdditions;
      } else {
        item.ProductAmount++;
        item.ProductAdditions = od.Product.Additions;
        item.AdditionsDisplay = SummaryItem.getDisplayNameAdditions(od.Product.Additions);
        sumAdditions = SummaryItem.getSumAdditions(item.ProductAdditions);
        item.SummaryProductPurchase = (item.ProductAmount * item.UnitPriceProduct) + sumAdditions;
        this.orderWithAddition.push(item);
      }
    } else {
      item.ProductAmount++;
      item.ProductAdditions = od.Product.Additions;
      item.AdditionsDisplay = SummaryItem.getDisplayNameAdditions(od.Product.Additions);
      sumAdditions = SummaryItem.getSumAdditions(item.ProductAdditions);
      item.SummaryProductPurchase = (item.ProductAmount * item.UnitPriceProduct) + sumAdditions;
      this.orderWithAddition.push(item);
    }
    console.log('adiciones');
    console.log(this.orderWithAddition);
  }

  private setSummaryItemWithOutAddtions(item: SummaryItem, od: OrderDetail) {
    const owa = this.orderWithOutAddition.filter(x => x.ProductId == od.Product.Id);

    if (owa.length > 0) {
      owa[0].ProductAmount++;
      owa[0].SummaryProductPurchase = owa[0].ProductAmount * owa[0].UnitPriceProduct;
    } else {
      item.ProductAmount++;
      item.SummaryProductPurchase = item.ProductAmount * item.UnitPriceProduct;
      this.orderWithOutAddition.push(item);
    }
  }

  private getSummaryOrderTotal() {
    this.clientOrder.OrderTotal = SummaryItem.getSummaryOrderTotal(this.orderWithAddition, this.orderWithOutAddition);
  }

  private showSelectedTableComponent() {
    this.selectedTableService.setCurrentTable(this.updateOrder ? this.clientOrder.Table : null);
    this.selectedTableService.show('Seleccionar mesa pedido',
      'La acción eliminará todos los productos seleccionados. ¿Desea continuar?')
      .then((confirmed) => {
        if (confirmed) {
          alert('confirmado');
          const tableSelected: Table = this.selectedTableService.getTableSelected();
          this.clientOrder.User = this.userLogged;
          this.clientOrder.Table = tableSelected;
          this.clientOrder.State = this.setStateOrder();
          this.clientOrder.CashTotal = this.clientOrder.OrderTotal;
          this.createOrUpdateOrder();
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  private createOrUpdateOrder() {
    if (this.updateOrder) {
      this.orderService.updateOrder(this.clientOrder).subscribe(
        result => {
          if (result.statusText === 'OK') {
            this.toastr.success('Pedido actualizado correctamente', 'Orden de compra', {
              positionClass: 'toast-bottom-right'
            });
            this.cleanObjects();
            this.router.navigate(['admin-orders']);
          }
          console.log(result);
        }, (e: HttpErrorResponse) => {
          if (e.error instanceof Error) {
            console.log('Error:' + e.message);
          } else {
            this.toastr.error('Error realizando pedido', 'Orden de compra', {
              positionClass: 'toast-bottom-right'
            });
            console.log(e.message);
          }
        }
      );
    } else {
      this.orderService.createOrder(this.clientOrder).subscribe(
        result => {
          if (result.statusText === 'OK') {
            this.toastr.success('Pedido creado correctamente', 'Orden de compra', {
              positionClass: 'toast-bottom-right'
            });
            this.cleanObjects();
            this.router.navigate(['admin-orders']);
          }
          console.log(result);
        }, (e: HttpErrorResponse) => {
          if (e.error instanceof Error) {
            console.log('Error:' + e.message);
          } else {
            this.toastr.error('Error realizando pedido', 'Orden de compra', {
              positionClass: 'toast-bottom-right'
            });
            console.log(e.message);
          }
        }
      );
    }
  }

  private setStateOrder(): State {
    var state: State = new State();

    if (this.updateOrder) {
      state.Id = 5;
      state.Name = 'Modificar Pedido';
    } else {
      state.Id = 1;
      state.Name = 'Crear Pedido';
      state.Enable = true;
    }

    return state;
  }

  public openConfirmationDialog() {
    this.confirmationDialogService.confirm('Orden de compra', 'La acción eliminará todos los productos seleccionados. ¿Desea continuar?')
      .then((confirmed) => {
        if (confirmed) {
          this.cleanObjects();
          this.router.navigate(['admin-orders']);
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }
}
