import { Category } from './category';
import { Addition } from './addition';

export class Product {
    Id: number;
    Name: string;
    Category: Category;
    Price: number;
    ImagePath: string;
    Description: string;
    Enable: boolean;
    Additions: Addition[];

    constructor() {
        this.Id = 0;
        this.Category = new Category();
        this.Price = 0;
        this.Additions = Array<Addition>();
    }
}
