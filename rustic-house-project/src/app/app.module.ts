import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppComponent } from './app.component';
import { DoOrdersComponent } from './do-orders/do-orders.component';
import { GroupByPipe } from './pipes/groupBy.pipe';
import { FilterPipe } from './pipes/Filter.pipe';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalServiceConfirmComponent } from './modal-service/modal-service-confirm.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';
import { AuthenticationComponent } from './authentication/authentication.component';
import { routing, appRoutingProviders } from './app.routing';
import { TableSelectedComponent } from './table-selected/table-selected.component';
import { TableSelectedService } from './table-selected/table-selected.service';
import { OrdersManagementComponent } from './orders-management/orders-management.component';
import { ViewOrderComponent } from './view-order/view-order.component';
import { ViewOrderService } from './view-order/view-order.service';
import { OrderPaymentComponent } from './order-payment/order-payment.component';
import { OrderPaymentService } from './order-payment/order-payment.service';

@NgModule({
  declarations: [
    AppComponent,
    DoOrdersComponent,
    GroupByPipe,
    FilterPipe,
    ModalServiceConfirmComponent,
    ConfirmationDialogComponent,
    AuthenticationComponent,
    TableSelectedComponent,
    OrdersManagementComponent,
    ViewOrderComponent,
    OrderPaymentComponent
  ],
  imports: [
    routing,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    HttpModule,
    ModalModule.forRoot(),
    NgbModule.forRoot(),
    CommonModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgxPaginationModule
  ],
  providers: [ConfirmationDialogService, appRoutingProviders, TableSelectedService, ViewOrderService, OrderPaymentService],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent, TableSelectedComponent, ViewOrderComponent, OrderPaymentComponent],
  exports: [ FilterPipe ]
})
export class AppModule { }
