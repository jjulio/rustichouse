import { TestBed, inject } from '@angular/core/testing';

import { DoOrdersService } from './do-orders.service';

describe('DoOrdersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DoOrdersService]
    });
  });

  it('should be created', inject([DoOrdersService], (service: DoOrdersService) => {
    expect(service).toBeTruthy();
  }));
});
