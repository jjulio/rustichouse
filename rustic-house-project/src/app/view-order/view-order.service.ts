import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Order } from '../order';
import { ViewOrderComponent } from './view-order.component';

@Injectable({
  providedIn: 'root'
})
export class ViewOrderService {
  private order: Order;

  constructor(private modalService: NgbModal) { }

  public show(
    title: string,
    message: string,
    btnOkText: string = 'Confirmar Pedido',
    btnPrintText: string = 'Imprimir Recibo',
    btnCloseText: string = 'Cerrar',
    dialogSize: 'sm' | 'lg' = 'lg'): Promise<boolean> {
    const modalRef = this.modalService.open(ViewOrderComponent, { size: dialogSize });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnPrintText = btnPrintText;
    modalRef.componentInstance.btnCloseText = btnCloseText;
    modalRef.componentInstance.order = this.order;
    return modalRef.result;
  }

  public setOrder(order: Order) {
    this.order = order;
  }
}
