import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorResponse } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Order } from '../order';
import { SummaryItem } from '../summary-item';
import { OrderDetail } from '../order-detail';
import { Utility } from '../utility';
import { DoOrdersService } from '../do-orders.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css'],
  providers: [DoOrdersService]
})
export class ViewOrderComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  @Input() btnOkText: string;
  @Input() btnPrintText: string;
  @Input() btnCloseText: string;
  @Input() order: Order;

  private orderWithAddition: Array<SummaryItem>;
  private orderWithOutAddition: Array<SummaryItem>;
  private summaryOrderTotal: number;

  constructor(private activeModal: NgbActiveModal,
    private orderService: DoOrdersService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.orderWithOutAddition = new Array<SummaryItem>();
    this.orderWithAddition = new Array<SummaryItem>();
    this.setSummaryItems();
  }

  private setSummaryItems() {
    var g = this.order.OrderDetails.reduce((x: any, od: OrderDetail) => {
      const item = new SummaryItem();
      item.ProductName = od.Product.Name;
      item.ProductId = od.Product.Id;
      item.UnitPriceProduct = od.Product.Price;

      if (od.Product.Additions.length > 0) {
        this.setSummaryItemWithAddtions(item, od);
      } else {
        this.setSummaryItemWithOutAddtions(item, od);
      }

      return x;
    }, {});

    this.getSummaryOrderTotal();
  }

  private setSummaryItemWithAddtions(item: SummaryItem, od: OrderDetail) {
    let sumAdditions = 0;
    const oa = this.orderWithAddition.filter(x => x.ProductId == od.Product.Id);

    if (oa.length > 0) {
      if (Utility.collectionCompare(oa[0].ProductAdditions, od.Product.Additions)) {
        oa[0].ProductAmount++;
        sumAdditions = SummaryItem.getSumAdditions(oa[0].ProductAdditions) * 2;
        oa[0].SummaryProductPurchase = (oa[0].ProductAmount * oa[0].UnitPriceProduct) + sumAdditions;
      } else {
        item.ProductAmount++;
        item.ProductAdditions = od.Product.Additions;
        sumAdditions = SummaryItem.getSumAdditions(item.ProductAdditions);
        item.SummaryProductPurchase = (item.ProductAmount * item.UnitPriceProduct) + sumAdditions;
        this.orderWithAddition.push(item);
      }
    } else {
      item.ProductAmount++;
      item.ProductAdditions = od.Product.Additions;
      sumAdditions = SummaryItem.getSumAdditions(item.ProductAdditions);
      item.SummaryProductPurchase = (item.ProductAmount * item.UnitPriceProduct) + sumAdditions;
      this.orderWithAddition.push(item);
    }
  }

  private setSummaryItemWithOutAddtions(item: SummaryItem, od: OrderDetail) {
    const owa = this.orderWithOutAddition.filter(x => x.ProductId == od.Product.Id);

    if (owa.length > 0) {
      owa[0].ProductAmount++;
      owa[0].SummaryProductPurchase = owa[0].ProductAmount * owa[0].UnitPriceProduct;
    } else {
      item.ProductAmount++;
      item.SummaryProductPurchase = item.ProductAmount * item.UnitPriceProduct;
      this.orderWithOutAddition.push(item);
    }
  }

  private getSummaryOrderTotal() {
    this.summaryOrderTotal = SummaryItem.getSummaryOrderTotal(this.orderWithAddition, this.orderWithOutAddition);
  }

  private paymentConfirm() {
    this.order.State.Id = parseInt(environment.STATE_CONFIRMED);
    this.orderService.updateStateOrder(this.order).subscribe(
      result => {
        if (result.statusText === 'OK') {
          this.toastr.success('El pedido se confirmó correctamente', 'Orden de compra', {
            positionClass: 'toast-bottom-right'
          });
          this.activeModal.close(true);
        }
      }, (e: HttpErrorResponse) => {
        if (e.error instanceof Error) {
          console.log('Error:' + e.message);
        } else {
          this.toastr.error('Error realizando pedido', 'Orden de compra', {
            positionClass: 'toast-bottom-right'
          });
          this.activeModal.close(false);
          console.log(e.message);
        }
      }
    );
  }

  private printTicket() {
    this.orderService.printTicket(this.order).subscribe(
      result => {
        if (result.statusText === 'OK') {
          this.toastr.success('El recibo de imprimío correctamente', 'Orden de compra', {
            positionClass: 'toast-bottom-right'
          });
          this.activeModal.close(true);
        }
      }, (e: HttpErrorResponse) => {
        if (e.error instanceof Error) {
          console.log('Error:' + e.message);
        } else {
          this.toastr.error('No se pudo imprimir el recibo', 'Orden de compra', {
            positionClass: 'toast-bottom-right'
          });
          this.activeModal.close(false);
          console.log(e.message);
        }
      }
    );
  }

  public accept() {
    this.activeModal.close(false);
  }

  public dismiss() {
    this.activeModal.close(false);
  }
}
