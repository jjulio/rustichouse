import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { AuthenticationService } from './authentication.service';
import { LoginRequest } from '../login-request';
import { User } from '../user';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css'],
  providers: [AuthenticationService]
})
export class AuthenticationComponent implements OnInit {
  private userName: string;
  private password: string;

  constructor(public authService: AuthenticationService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

  }

  authenticateUser(): void {
    if (!this.validate()) {
      this.toastr.warning('usuario y contraseña son requeridos', 'Login', {
        positionClass: 'toast-bottom-right',
      });
      return;
    }

    const userCredential: LoginRequest = { Username: this.userName, Password: this.password };
    this.authService.authenticate(userCredential).subscribe(
      result => {
        let user: User = new User();
        user = <User>result;
        this.router.navigate(['admin-orders']);
      },
      (error: HttpErrorResponse) => {
        if (error.error instanceof Error) {
          console.log('Error:' + error.message);
        } else {
          this.toastr.error('usuario o contraseña invalida', 'Login', {
            positionClass: 'toast-bottom-right'
          });
        }
      }
    );

    console.log('localStorage');;
    console.log(this.authService.getUserLoggedIn());
  }

  validate(): boolean {
    if (!this.userName || !this.password) {
      return false;
    }

    if (this.userName.length > 0 && this.password.length > 0) {
      return true;
    }
  }

  WindowOpen() {
    this.GotoSigo();
  }



  GotoSigo(): any {
    var configuracion_ventana = "menubar=yes,location=yes,resizable=no,scrollbars=no,status=yes";
    var win = window.open("http://localhost:3903/Administracion/OSE/CompanyManagement.aspx", "SIGO", configuracion_ventana);
    win.postMessage("Felipito Julio", "http://localhost:3903/Administracion/OSE/CompanyManagement.aspx");

    win.addEventListener("message", function (event) {
      if (event.origin !== 'http://localhost:3903') return;

      console.log('message received:  ' + event.data, event);
      //event.source.postMessage('holla back youngin!', event.origin);
    }, false);

    var timer = setInterval(function () {
      if (win.closed) {
        clearInterval(timer);
        //var returnValue = win.localStorage.getItem("Felipito");

        //alert(returnValue);
        //callback(returnValue);
      }
    }, 500);
  }
}
