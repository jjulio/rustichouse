import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { LoginRequest } from '../login-request';
import { User } from '../user';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {
  private urlResource: string;
  private isUserLoggedIn: boolean;
  public userLogged: User;

  constructor(private http: Http) { }

  authenticate(userCredential: LoginRequest): Observable<User> {
    this.setUserLoggedIn(null);
    this.urlResource = environment.serverUrl + environment.authPath;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.urlResource, userCredential, { headers: headers })
      .map(response => { this.setUserLoggedIn(response.json()); return response.json() });
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return <any>(result as T);
    };
  }

  private log(message: string) {
    console.log('AuthenticationService: ' + message);
  }

  private setUserLoggedIn(user: User) {
    if (user === null) {
      !this.isUserLoggedIn;
      localStorage.removeItem('currentUser');
      return;
    }

    this.isUserLoggedIn = true;
    this.userLogged = user;
    localStorage.setItem('currentUser', JSON.stringify(user));
  }

  public getUserLoggedIn(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }
}
