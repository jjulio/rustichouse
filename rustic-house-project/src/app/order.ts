import { User } from './user';
import { State } from './state';
import { OrderDetail } from './order-detail'
import { Table } from './models/table';

export class Order {
    Id: number;
    User: User;
    State: State;
    OrderTotal: number;
    OrderDate: Date;
    OrderDetails: OrderDetail[];
    Table: Table;
    Tip: number;
    CashTotal: number;
    CardTotal: number;
    Voucher: string;
    Discount: number;

    constructor() {
        this.Id = 0;
        this.User = new User();
        this.State = new State();
        this.OrderTotal = 0;
        this.OrderDate = new Date();
        this.OrderDetails = new Array<OrderDetail>();
        this.Table = new Table();
        this.Tip = 0;
        this.CashTotal = 0;
        this.CardTotal = 0;
        this.Discount = 0;
    }
}
