import { Product } from './product';
import { Pipe, PipeTransform } from '@angular/core'

export class OrderDetail{
    Id: number;
    Product: Product;

    constructor(){
        this.Id = 0;
        this.Product = new Product();
    }   
}
