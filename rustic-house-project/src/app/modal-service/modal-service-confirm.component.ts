import { Component, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'modal-confirm',
  templateUrl: './modal-service-confirm.component.html',
  styleUrls: ['./modal-service-confirm.component.css']
})
export class ModalServiceConfirmComponent {
  modalRef: BsModalRef;
  private message: string;
  private btnName: string;
  private title: string;
  private btnOkText: string;
  private btnCancelText: string;

  constructor(private modalService: BsModalService) { }

  setProperties(buttonName: string, title: string, okText: string, cancelText: string, message: string): void {
    this.btnName = buttonName;
    this.title = title;
    this.btnOkText = okText;
    this.btnCancelText = cancelText;
    this.message = message;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  confirm(): void {
    this.modalRef.hide();
  }

  decline(): void {
    this.modalRef.hide();
  }

  dismiss(): void {
    this.modalRef.hide();
  }
}

