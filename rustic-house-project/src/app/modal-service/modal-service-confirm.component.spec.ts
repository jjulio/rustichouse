import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalServiceConfirmComponent } from './modal-service-confirm.component';

describe('ModalServiceComponent', () => {
  let component: ModalServiceConfirmComponent;
  let fixture: ComponentFixture<ModalServiceConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalServiceConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalServiceConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
