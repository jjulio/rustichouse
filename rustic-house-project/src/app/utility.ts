import { Addition } from './addition';

export class Utility {

    public static collectionCompare2(collection1: Array<any>, collection2: Array<any>): boolean {
        if (!collection1 || !collection2) {
            return false;
        }



        for (let i = 0; i < collection1.length; i++) {
            if (collection1[i] instanceof Array && collection2[i] instanceof Array) {
                if (!collection1[i].equals(collection2[i])) {
                    return false;
                }
            } else if (collection1[i] !== collection2[i]) {
                return false;
            }
        }

        return true;
    }

    public static collectionCompare(collection1: Array<Addition>, collection2: Array<Addition>) {
        collection1 = collection1.sort(x => x.Id);
        collection2 = collection2.sort(x => x.Id);

        if (!collection1 && !collection2) {
            alert(1);
            return false;
        }

        if (collection1.length !== collection2.length) {
            alert('2 ::: col1: ' + collection1.length + ' col2: ' + collection2.length);
            return false;
        }

        for (let index1 = 0; index1 < collection1.length; index1++) {
            //for (let index2 = 0; index2 < collection2.length; index2++) {
            if (collection1[index1].Id !== collection2[index1].Id) {
                alert('3 ::: ' + collection1[index1].Id + ' - ' + collection2[index1].Id);
                return false;
            }
            //}
        }

        return true;
    }

    public static getPropertyNames(obj): any {
        const proto = Object.getPrototypeOf(obj);
        return (
            (typeof proto === 'object' && proto !== null ? this.getPropertyNames(proto) : [])
                .concat(Object.getOwnPropertyNames(obj))
                .filter(function (item, pos, result) { return result.indexOf(item) === pos; })
                .sort()
        );

        return proto;
    }

    private static padLeft(base: number, value: number) {
        return String(value).length === 1 ? String(value).padStart(base, '0') : value;
    }

    private static hourFormat(date: Date): string {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'

        minutes = minutes < 10 ? 0 + minutes : minutes;
        var strTime = hours + ':' + minutes + ':' + seconds;

        return strTime;
    }

    public static getStringDate(date: Date): string {
        var stringDate = [Utility.padLeft(2, date.getDate()),
        Utility.padLeft(2, date.getMonth() + 1),
        date.getFullYear()].join('/') + ' ' +
            this.hourFormat(date);
        return stringDate;
    }
}
