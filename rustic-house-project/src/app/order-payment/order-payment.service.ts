import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Order } from '../order';
import { OrderPaymentComponent } from './order-payment.component';

@Injectable({
  providedIn: 'root'
})
export class OrderPaymentService {
  private order: Order;

  constructor(private modalService: NgbModal) { }

  public show(
    title: string,
    message: string,
    btnDoPayment: string = 'Realizar Pago',
    btnCloseText: string = 'Cerrar',
    dialogSize: 'sm' | 'lg' = 'lg'): Promise<boolean> {
    const modalRef = this.modalService.open(OrderPaymentComponent, { size: dialogSize });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.btnDoPayment = btnDoPayment;
    modalRef.componentInstance.btnCloseText = btnCloseText;
    modalRef.componentInstance.order = this.order;
    return modalRef.result;
  }

  public setOrder(order: Order) {
    this.order = order;
  }
}
