import { Component, Input, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Order } from '../order';
import { environment } from '../../environments/environment';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { ToastrService } from 'ngx-toastr';
import { DoOrdersService } from '../do-orders.service';

@Component({
  selector: 'app-order-payment',
  templateUrl: './order-payment.component.html',
  styleUrls: ['./order-payment.component.css'],
  providers: [DoOrdersService, CurrencyPipe]
})
export class OrderPaymentComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  @Input() btnDoPayment: string;
  @Input() btnCloseText: string;
  @Input() order: Order;

  private orderTotalWithTip: number = 0;
  private change: number = 0;
  private ispaycard: boolean;

  constructor(private activeModal: NgbActiveModal,
    private orderService: DoOrdersService,
    private currencyPipe: CurrencyPipe,
    private confirmationDialogService: ConfirmationDialogService,
    private toastr: ToastrService) { }

  ngOnInit() {
    console.log(this.order);
    this.order.OrderTotal = this.calculateOrderTotal();
    this.order.Tip = this.calculateTip();
    this.orderTotalWithTip = Math.ceil(this.order.OrderTotal + this.order.Tip);
    this.order.CashTotal = 0;
  }

  private doPayment() {
    this.confirmationDialogService.confirm('Orden de compra', 'Desea imprimir el recibo?')
      .then((confirmed) => {
        if (confirmed) {
          this.paymentValidate();
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  private calculateOrderTotal(): number {
    let additionsCost: number = 0;
    let summaryProductPurchase: number = 0;
    this.order.OrderDetails.forEach(x => {
      additionsCost = x.Product.Additions.reduce((previous, current) => {
        return previous + current.Price;
      }, 0);

      summaryProductPurchase += x.Product.Price + additionsCost;
      additionsCost = 0;
    });

    return summaryProductPurchase;
  }

  private calculateTip(): number {
    return Math.ceil(this.order.OrderTotal * parseInt(environment.percentTips) / 100);
  }

  private modelChanged(event: any) {
    switch (event.target.name) {
      case 'tip':
        this.order.Tip = parseInt(event.target.value);
        break;
      case 'discount':
        this.order.Discount = parseInt(event.target.value);
        this.orderTotalWithTip -= this.order.Discount;
        break;
      case 'orderTotalWithTip':
        this.orderTotalWithTip = parseInt(event.target.value);
        break;
      case 'cashTotal':
        this.order.CashTotal = parseInt(event.target.value);
        break;
      case 'cardTotal':
        this.order.CardTotal = parseInt(event.target.value);
        this.ispaycard = true;
        break;
    }

    this.change = (this.order.CashTotal + this.order.CardTotal) - this.orderTotalWithTip;
  }

  private paymentValidate() {
    let result: boolean = false;

    if (this.order.CashTotal + this.order.CardTotal < this.orderTotalWithTip) {
      this.toastr.error('No se ha completado el valor de la compra', 'Orden de compra', {
        positionClass: 'toast-bottom-right'
      });
    } else {
      result = true;
    }

    if (this.order.CardTotal > 0 && !this.order.Voucher) {
      this.toastr.error('Se debe ingresar el número del voucher del pago con tarjeta', 'Orden de compra', {
        positionClass: 'toast-bottom-right'
      });
    } else {
      result = true;
    }

    if (this.change < 0) {
      this.toastr.error('No se ha completado el valor de la compra', 'Orden de compra', {
        positionClass: 'toast-bottom-right'
      });
    } else {
      result = true;
    }

    if (this.change > 100000) {
      this.toastr.warning('Por favor verifique el monto ingresado ya que es mucho mayor al valor a pagar', 'Orden de compra', {
        positionClass: 'toast-bottom-right'
      });
    } else {
      result = true;
    }

    return result;
  }

  public accept() {
    this.activeModal.close(false);
  }

  public dismiss() {
    this.activeModal.close(false);
  }
}
