import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthenticationComponent } from './authentication/authentication.component';
import { DoOrdersComponent } from './do-orders/do-orders.component';
import { OrdersManagementComponent } from './orders-management/orders-management.component';

const appRoutes: Routes = [
    { path: '', component: AuthenticationComponent },
    { path: 'login', component: AuthenticationComponent },
    { path: 'do-orders', component: DoOrdersComponent },
    { path: 'do-orders/:id',  component: DoOrdersComponent },
    { path: 'admin-orders', component: OrdersManagementComponent },
    { path: '**', component: AuthenticationComponent }
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);