import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';

import { DoOrdersService } from '../do-orders.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { ViewOrderService } from '../view-order/view-order.service';
import { OrderPaymentService } from '../order-payment/order-payment.service';
import { ToastrService } from 'ngx-toastr';
import { Order } from '../order';
import { User } from '../user';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-orders-management',
  templateUrl: './orders-management.component.html',
  styleUrls: ['./orders-management.component.css'],
  providers: [DoOrdersService, AuthenticationService, ViewOrderService, OrderPaymentService]
})
export class OrdersManagementComponent implements OnInit {

  private orders: Array<Order>;
  private userLogged: User;
  public searchText: string;

  constructor(private orderService: DoOrdersService,
    private confirmationDialogService: ConfirmationDialogService,
    private authService: AuthenticationService,
    private viewOrderService: ViewOrderService,
    private OrderPaymentService: OrderPaymentService,
    private toastr: ToastrService,
    private router: Router) {
    if (!this.validateUser()) {
      this.router.navigate(['login']);
      return;
    }
  }

  ngOnInit() {
    this.getOrders();
    this.searchText = '';
  }

  private validateUser(): boolean {
    this.userLogged = this.authService.getUserLoggedIn();
    return !this.userLogged || this.userLogged === null ? false : true;
  }

  private getOrders() {
    this.orderService.getAllOrdersActives(this.userLogged).subscribe(
      result => {
        this.orders = result;
      },
      (e: HttpErrorResponse) => {
        if (e.error instanceof Error) {
          console.log('Error:' + e.message);
        } else {
          this.toastr.warning('La sessión caducó, ingrese nuevamente sus credenciales', 'Autenticación', {
            positionClass: 'toast-bottom-right'
          });
          this.router.navigate(['login']);
        }
      }
    );
  }

  private orderCancel(order: Order) {
    this.confirmationDialogService.confirm('Administración de Pedidos', 'La acción cancelará el pedido. ¿Desea continuar?')
      .then((confirmed) => {
        if (confirmed) {
          order.User = this.userLogged;
          order.State.Id = parseInt(environment.STATE_CANCELED);
          this.updateOrder(order);
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  private viewOrder(order: Order) {
    order.User = this.userLogged;
    this.viewOrderService.setOrder(order);
    this.viewOrderService.show('Administración de Pedidos', 'Detalles Pedido')
      .then((confirmed) => {
        if (confirmed) {
          this.getOrders();
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  private doOrderPayment(order: Order) {
    order.User = this.userLogged;
    this.OrderPaymentService.setOrder(order);
    this.OrderPaymentService.show('Administración de Pedidos', 'Pago del Pedido')
      .then((confirmed) => {
        if (confirmed) {
          //this.getOrders();
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  private updateOrder(order: Order) {
    this.orderService.updateStateOrder(order).subscribe(
      result => {
        if (result.statusText === 'OK') {
          this.toastr.success('El pedido se canceló correctamente', 'Orden de compra', {
            positionClass: 'toast-bottom-right'
          });
          this.getOrders();
        }
      }, (e: HttpErrorResponse) => {
        if (e.error instanceof Error) {
          console.log('Error:' + e.message);
        } else {
          this.toastr.error('Error No se pudo cancelar el pedido', 'Orden de compra', {
            positionClass: 'toast-bottom-right'
          });
          console.log(e.message);
        }
      }
    );
  }
}
