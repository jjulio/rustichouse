import { Addition } from './addition';

export class SummaryItem {
    ProductId: number;
    ProductName: string;
    ProductAmount: number;
    ProductDescription: string;
    UnitPriceProduct: number;
    AdditionsDisplay: string;
    SummaryProductPurchase: number;
    ProductAdditions: Array<Addition>;

    constructor() {
        this.ProductId = 0;
        this.ProductAmount = 0;
        this.UnitPriceProduct = 0;
        this.SummaryProductPurchase = 0;
        this.ProductAdditions = new Array<Addition>();
        this.AdditionsDisplay = '';
    }

    public static getSummaryPurchase(additions: Array<Addition>, productAmount: number, unitPriceProduct: number): number {
        if (additions != null) {
            var purchase = additions.reduce((previous, current) => {
                return previous + current.Price;
            }, 0);
        }

        return (productAmount * unitPriceProduct) + (purchase * productAmount);
    }

    public static getSumAdditions(additions: Array<Addition>) {
        let sumAdditions = 0;
        if (additions != null) {
            sumAdditions = additions.reduce((previous, current) => {
                return previous + current.Price;
            }, 0);
        }

        return sumAdditions;
    }

    public static getSummaryOrderTotal(orderWithAddition: Array<SummaryItem>, orderWithOutAddition: Array<SummaryItem>): number {
        const summaryWithAddition = orderWithAddition.reduce((previous, current) => {
            return previous + current.SummaryProductPurchase;
        }, 0);

        const summaryWithOutAddition = orderWithOutAddition.reduce((previous, current) => {
            return previous + current.SummaryProductPurchase;
        }, 0);

        return summaryWithAddition + summaryWithOutAddition;
    }

    public static getDisplayNameAdditions(additions: Array<Addition>): string {
        let additionDisplayName: string = '';
        if (additions != null) {
            additions.forEach(element => {
                additionDisplayName = additionDisplayName.length > 0 ? `${additionDisplayName},${element.Name}` : element.Name;
            });
        }
        
        return additionDisplayName;
    }
}
