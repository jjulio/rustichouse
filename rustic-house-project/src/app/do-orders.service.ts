import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { Category } from './category';
import { Order } from './order';
import { User } from './user';
import { environment } from '../environments/environment';


@Injectable()
export class DoOrdersService {
  private urlResource: string;

  constructor(private http: Http) { }

  public getAllData(): Observable<Array<Category>> {
    this.urlResource = environment.serverUrl + environment.getAllDataPath;
    return this.http.get(this.urlResource).map(response => response.json());
  }

  public createOrder(order: Order): Observable<any> {
    this.urlResource = environment.serverUrl + environment.createOrderPath;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', order.User.Token);
    return this.http.post(this.urlResource, JSON.stringify(order), { headers: headers });
  }

  public updateOrder(order: Order): Observable<any> {
    this.urlResource = environment.serverUrl + environment.updateOrderPath;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', order.User.Token);
    return this.http.post(this.urlResource, JSON.stringify(order), { headers: headers });
  }

  public getAllOrdersActives(user: User): Observable<Array<Order>> {
    this.urlResource = environment.serverUrl + environment.getAllOrdersActives;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', user.Token);
    return this.http.get(this.urlResource, { headers: headers }).map(response => response.json());
  }

  public updateStateOrder(order: Order): Observable<any> {
    var params = new Map();
    params.set('idOrder', order.Id);
    params.set('idState', order.State.Id);

    let urlResource = environment.serverUrl + environment.updateStateOrder + '?';
    let parameters = { 'idOrder': order.Id, 'idState': order.State.Id };

    params.forEach(function (value, key, element) {
      urlResource += `${key}=${value}&`;
    });

    urlResource = urlResource.substring(0, urlResource.length - 1);

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', order.User.Token);
    return this.http.post(urlResource, JSON.stringify(parameters), { headers: headers });
  }

  public printTicket(order: Order) {
    let urlResource: string = '';
    urlResource = environment.serverUrl + environment.printTicket;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', order.User.Token);
    return this.http.post(urlResource, JSON.stringify(order), { headers: headers });
  }

  public getOrder(id: number, token: string): Observable<Order> {
    this.urlResource = `${environment.serverUrl}${environment.getOrderById}/${id}`;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', token);
    return this.http.get(this.urlResource, { headers: headers }).map(response => response.json());
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return <any>(result as T);
    };
  }

  private log(message: string) {
    console.log('UserService: ' + message);
  }
}
