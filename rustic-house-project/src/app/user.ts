import { Profile } from '././profile';

export class User {
    Id: number;
    UserName: string;
    Password: string;
    Profile: Profile;
    Enable: boolean;
    Token: string;

    constructor() {
        this.Profile = new Profile();
    }
}
