import { Pipe, PipeTransform, Injectable } from '@angular/core';
import { Utility } from '../utility';


@Pipe({ name: 'filter' })
@Injectable()
export class FilterPipe implements PipeTransform {
    transform(items: Array<any>, field: string, value: string): any {
        if (!items) {
            return [];
        }

        if (!field || !value) {
            return items;
        }

        return items.filter(singleItem =>
            Utility.getStringDate(new Date(singleItem.OrderDate)).toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
            singleItem.User.UserName.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
            singleItem.Table.Name.toLowerCase().indexOf(value.toLowerCase()) !== -1 ||
            singleItem.State.Name.toLowerCase().indexOf(value.toLowerCase()) !== -1);
    }
}