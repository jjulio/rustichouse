import { Product } from './product';
import { Addition } from './addition';

export class Category {
    Id: number;
    Name: string;
    Enable: boolean;
    ImagePath: string;
    Products: Product[];
    Additions: Addition[];

    constructor() {
        this.Id = 0;
        this.Products = new Array<Product>();
        this.Additions = new Array<Addition>();
    };   
}
