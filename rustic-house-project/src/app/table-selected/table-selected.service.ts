import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { TableSelectedComponent } from './table-selected.component';
import { AuthenticationService } from '../authentication/authentication.service';
import { Observable } from 'rxjs';
import { Table } from '../models/table';
import { environment } from '../../environments/environment';
import { User } from '../user';

@Injectable({
  providedIn: 'root'
})
export class TableSelectedService {
  private urlResource: string;
  private currentTable?: Table;

  constructor(private modalService: NgbModal, private http: Http, private authService: AuthenticationService) { }

  public show(
    title: string,
    message: string,
    btnOkText: string = 'Aceptar',
    btnCancelText: string = 'Cancelar',
    dialogSize: 'sm' | 'lg' = 'lg'): Promise<boolean> {
    const modalRef = this.modalService.open(TableSelectedComponent, { size: dialogSize });
    modalRef.componentInstance.title = title;
    modalRef.componentInstance.message = message;
    modalRef.componentInstance.btnOkText = btnOkText;
    modalRef.componentInstance.btnCancelText = btnCancelText;

    this.getAllTables().subscribe(
      result => {
        if (this.currentTable != null) {
          let tb = result.find(x => x.Id === this.currentTable.Id).Checked = this.currentTable.Checked;
          modalRef.componentInstance.currentTable = this.currentTable;
        }

        modalRef.componentInstance.tables = result;
      }
    );

    return modalRef.result;
  }

  public getAllTables(): Observable<Array<Table>> {
    const user = <User>JSON.parse(localStorage.getItem('currentUser'));
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', user.Token);
    this.urlResource = environment.serverUrl + environment.getAllTablePath;
    return this.http.get(this.urlResource, { headers: headers }).map(response => response.json());
  }

  public getTableSelected(): Table {
    if (this.currentTable != null) {
      return TableSelectedComponent.getTableSelected(this.currentTable);
    } else {
      return TableSelectedComponent.getTableSelected(null);
    }

    // console.log(<Table>JSON.parse(localStorage.getItem('tableSelected')));
    // return this.currentTable != null ? this.currentTable : TableSelectedComponent.getTableSelected();
  }

  public setCurrentTable(table: Table) {
    if (table != null) {
      table.Checked = true;
    }

    this.currentTable = table;
  }
}
