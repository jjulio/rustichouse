import { TestBed } from '@angular/core/testing';

import { TableSelectedService } from './table-selected.service';

describe('TableSelectedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TableSelectedService = TestBed.get(TableSelectedService);
    expect(service).toBeTruthy();
  });
});
