import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Table } from '../models/table';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-table-selected',
  templateUrl: './table-selected.component.html',
  styleUrls: ['./table-selected.component.css'],
  providers: []
})
export class TableSelectedComponent implements OnInit {
  @Input() title: string;
  @Input() message: string;
  @Input() btnOkText: string;
  @Input() btnCancelText: string;
  @Input() tables: Table[];
  @Input() currentTable?: Table;
  @Output() prueba: EventEmitter<Table> = new EventEmitter();;

  private tableSelected: Table;

  constructor(private activeModal: NgbActiveModal) {
    this.tableSelected = new Table();
  }

  ngOnInit() {
    // console.log(this.currentTable);
    // console.log('currentable:' + this.currentTable != null ? this.currentTable.Id : 0);
    // console.log('tableSelected:' + this.tableSelected != null ? this.tableSelected.Id : 0);

    // if (this.currentTable != null) {
    //   this.tableSelected = this.currentTable;
    //   localStorage.setItem('tableSelected', JSON.stringify(this.currentTable));
    // } else {
    //   this.tableSelected = new Table();
    //   localStorage.removeItem('tableSelected');
    // }

    if (<Table>JSON.parse(localStorage.getItem('tableSelected')) != null) {
      this.currentTable = <Table>JSON.parse(localStorage.getItem('tableSelected'));
      this.currentTable = this.tableSelected;
    } else {
      this.tableSelected = new Table();
      localStorage.removeItem('tableSelected');
    }
  }

  public decline() {
    this.activeModal.close(false);
  }

  public accept() {
    this.activeModal.close(true);
  }

  public dismiss() {
    this.activeModal.dismiss();
  }

  private applyChanges(table: Table) {
    if (this.currentTable != null) {
      this.tableSelected = this.currentTable;
    }

    if (this.tableSelected.Id != 0) {
      if (this.tableSelected.Id != table.Id) {
        this.tables.filter(x => x.Id === this.tableSelected.Id)[0].Checked = false;
      }
    }

    table.Checked = !table.Checked;
    this.tableSelected = table;
    this.currentTable = this.currentTable != null ? this.tableSelected : null;

    console.log(this.tables);
    localStorage.setItem('tableSelected', JSON.stringify(this.tableSelected));
  }

  public static getTableSelected(tableService: Table): Table {
    if (tableService != null) {
      return <Table>JSON.parse(localStorage.getItem('tableSelected'));
    } else {
      return <Table>JSON.parse(localStorage.getItem('tableSelected'));
    }
  }
}
