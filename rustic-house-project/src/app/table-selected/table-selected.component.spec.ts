import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableSelectedComponent } from './table-selected.component';

describe('TableSelectedComponent', () => {
  let component: TableSelectedComponent;
  let fixture: ComponentFixture<TableSelectedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableSelectedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableSelectedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
